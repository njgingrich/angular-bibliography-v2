import { Component, OnInit } from '@angular/core';
import { Bibentry } from '../../models/bibentry.model';
import { RepositoryService } from '../../services/repository.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bibentry-editor',
  templateUrl: './bibentry-editor.component.html',
  styleUrls: ['./bibentry-editor.component.css']
})
export class BibentryEditorComponent implements OnInit {

  private id : number;
  private book : Bibentry;
  private statusMessage: string;
  private state : string;
  private classMap : object;

  constructor(private _repository : RepositoryService, private _router : Router, private _activatedRoute: ActivatedRoute) {
    this.book = new Bibentry();
  }

  ngOnInit() {
    this.id = +this._activatedRoute.snapshot.paramMap.get("id");
    console.log(this.id ? `Editing entry with id ${this.id}` : 'Creating a new entry');
    if (this.id) {
      this._repository.getBookById(this.id).subscribe (book => this.book = book);
    }
    this.state = "editing";
  }

  addNewBook () : void {
    debugger
    this.statusMessage = null;
    this.state = "updating";

    if (!this.id) {
      this.statusMessage = 'Saving new book';
      this._repository.addBook(this.book);
      this.book = new Bibentry();
      this._router.navigateByUrl('/');
    }
    else {
      this.statusMessage = 'Updating book';
      this._repository.updateBook(this.book).subscribe(
        val => {
          if (val) {
            this._router.navigateByUrl('/');
          }
          else {
            this.state = "error";
            this.statusMessage = 'An error occurred updating the book';
            setTimeout( () => this.statusMessage = null, 3000);
          }
        }
      );
    }

  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BibentryComponent } from './components/bibentry/bibentry.component';
import { FirstInitialPipe } from './pipes/first-initial.pipe';
import { BibentryEditorComponent } from './components/bibentry-editor/bibentry-editor.component';
import { AppRoutingModule } from './/app-routing.module';
import { BibliographyComponent } from './components/bibliography/bibliography.component';

import { RepositoryService } from './services/repository.service';

@NgModule({
  declarations: [
    AppComponent,
    BibentryComponent,
    FirstInitialPipe,
    BibentryEditorComponent,
    BibliographyComponent
  ],
  imports: [
    BrowserModule, FormsModule, AppRoutingModule
  ],
  providers: [RepositoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }

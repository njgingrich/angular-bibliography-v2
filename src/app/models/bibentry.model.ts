export class Bibentry {
  title : string;
  authorFirst : string;
  authorLast : string;
  year: number;
  publisher: string;
  id: number;
}

import { NgModule } from '@angular/core';

import { BibentryEditorComponent } from './components/bibentry-editor/bibentry-editor.component';
import { BibliographyComponent } from './components/bibliography/bibliography.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    'path': 'new',
    'component': BibentryEditorComponent
  },
  {
    'path': 'edit/:id',
    'component': BibentryEditorComponent
  },
  {
    'path': '',
    'component': BibliographyComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
